import RAMDirectoryT from './RAMDirectory'
export namespace store {
  export type RAMDirectory = RAMDirectoryT
}
export const store = {
  RAMDirectory: RAMDirectoryT
}