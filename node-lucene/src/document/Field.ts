import { IndexableField } from '../index/IndexableField';
import { lang } from 'node-java-rt';

export class Field extends lang.Object implements IndexableField {
  // /**
  //  * (Ljava/lang/String;Ljava/lang/String;Lorg/apache/lucene/index/IndexableFieldType;)
  //  */
  // new(arg0: string, arg1: string, arg2: any /*org.apache.lucene.index.IndexableFieldType*/);
  // /**
  //  * (Ljava/lang/String;Lorg/apache/lucene/index/IndexableFieldType;)
  //  */
  // new(arg0: string, arg1: any /*org.apache.lucene.index.IndexableFieldType*/);
  // /**
  //  * (Ljava/lang/String;Lorg/apache/lucene/analysis/TokenStream;Lorg/apache/lucene/index/IndexableFieldType;)
  //  */
  // new(arg0: string, arg1: any /*org.apache.lucene.analysis.TokenStream*/, arg2: any /*org.apache.lucene.index.IndexableFieldType*/);
  // /**
  //  * (Ljava/lang/String;[BLorg/apache/lucene/index/IndexableFieldType;)
  //  */
  // new(arg0: string, arg1: any /*byte[]*/, arg2: any /*org.apache.lucene.index.IndexableFieldType*/);
  // /**
  //  * (Ljava/lang/String;Ljava/io/Reader;Lorg/apache/lucene/index/IndexableFieldType;)
  //  */
  // new(arg0: string, arg1: any /*java.io.Reader*/, arg2: any /*org.apache.lucene.index.IndexableFieldType*/);
  // /**
  //  * (Ljava/lang/String;Lorg/apache/lucene/util/BytesRef;Lorg/apache/lucene/index/IndexableFieldType;)
  //  */
  // new(arg0: string, arg1: any /*org.apache.lucene.util.BytesRef*/, arg2: any /*org.apache.lucene.index.IndexableFieldType*/);
  // /**
  //  * (Ljava/lang/String;[BIILorg/apache/lucene/index/IndexableFieldType;)
  //  */
  // new(arg0: string, arg1: any /*byte[]*/, arg2: number, arg3: number, arg4: any /*org.apache.lucene.index.IndexableFieldType*/);
  // /**
  //  * ()Lorg/apache/lucene/util/BytesRef;
  //  */
  // binaryValue(): any /*org.apache.lucene.util.BytesRef*/;
  // /**
  //  * (Lorg/apache/lucene/analysis/Analyzer;Lorg/apache/lucene/analysis/TokenStream;)Lorg/apache/lucene/analysis/TokenStream;
  //  */
  // tokenStream(arg0: any /*org.apache.lucene.analysis.Analyzer*/, arg1: any /*org.apache.lucene.analysis.TokenStream*/): any /*org.apache.lucene.analysis.TokenStream*/;
  // /**
  //  * ()Ljava/lang/String;
  //  */
  // stringValue(): string;
  // /**
  //  * ()Ljava/lang/Number;
  //  */
  // numericValue(): any /*java.lang.Number*/;
  // /**
  //  * ()Ljava/io/Reader;
  //  */
  // readerValue(): any /*java.io.Reader*/;
  // /**
  //  * ()Ljava/lang/String;
  //  */
  // name(): string;
  // /**
  //  * ()Ljava/lang/String;
  //  */
  // toString(): string;
  // /**
  //  * (Ljava/io/Reader;)V
  //  */
  // setReaderValue(arg0: any /*java.io.Reader*/): any /*void*/;
  // /**
  //  * ()Lorg/apache/lucene/analysis/TokenStream;
  //  */
  // tokenStreamValue(): any /*org.apache.lucene.analysis.TokenStream*/;
  // /**
  //  * (Lorg/apache/lucene/util/BytesRef;)V
  //  */
  // setBytesValue(arg0: any /*org.apache.lucene.util.BytesRef*/): any /*void*/;
  // /**
  //  * ([B)V
  //  */
  // setBytesValue(arg0: any /*byte[]*/): any /*void*/;
  // /**
  //  * (B)V
  //  */
  // setByteValue(arg0: any /*byte*/): any /*void*/;
  // /**
  //  * (S)V
  //  */
  // setShortValue(arg0: any /*short*/): any /*void*/;
  // /**
  //  * (I)V
  //  */
  // setIntValue(arg0: number): any /*void*/;
  // /**
  //  * (F)V
  //  */
  // setFloatValue(arg0: number): any /*void*/;
  // /**
  //  * (D)V
  //  */
  // setDoubleValue(arg0: number): any /*void*/;
  // /**
  //  * (Lorg/apache/lucene/analysis/TokenStream;)V
  //  */
  // setTokenStream(arg0: any /*org.apache.lucene.analysis.TokenStream*/): any /*void*/;
  // /**
  //  * ()Lorg/apache/lucene/index/IndexableFieldType;
  //  */
  // fieldType(): any /*org.apache.lucene.index.IndexableFieldType*/;
  // /**
  //  * (J)V
  //  */
  // setLongValue(arg0: number): any /*void*/;
  // /**
  //  * (Ljava/lang/String;)V
  //  */
  // setStringValue(arg0: string): any /*void*/;
}